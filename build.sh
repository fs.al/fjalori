#!/bin/bash
### Generate HTML and JS files of the aplication by including partial files.
###
### Brace Tags can be installed with: `sudo pip2 install brace-tags`
### or with: `pip2 install -e path/to/your/brace-tags/folder`
### For more details see: https://github.com/braceio/tags
### or: https://pypi.org/project/brace-tags/

### Generate HTML files. 
tags build --root html/ --output .

### Generate JavaScript files.
tags build --root js/ --output . \
           --files '**/*.js' --force
