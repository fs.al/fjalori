# vocabulary

Vocabulary responsive web app, built with jQuery Mobile.

The data are received through B-Translator API:
 - http://info.btr.fs.al/api-examples-js/
 - http://info.btr.fs.al/api/


## Building HTML files

HTML files are split into several pieces and templates for easy development
and maintenance. To assemble them use: `./build.sh`
